<?php
/**
 * Plugin Name: Consulting - Functions
 * Plugin URI:	http://www.webideaperu.net
 * Description: Funciones necesarias para el funcionamiento del tema Consulting
 * Version: 1.1
 * Author:	Gustavo Pajuelo Vargas
 * Author URI: http://www.webideaperu.net
 */

defined( 'ABSPATH' ) || exit();

// Global constants.
if ( ! defined( 'WIP_PLUGIN_URL' ) ) define( 'WIP_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
if ( ! defined( 'WIP_PLUGIN_PATH' ) ) define('WIP_PLUGIN_PATH', plugin_dir_path( __FILE__ ));
if ( ! defined( 'WIP_PLUGIN_FILE' ) ) define('WIP_PLUGIN_FILE', __FILE__ );
if ( ! defined( 'WIP_PREFIX' ) ) define('WIP_PREFIX', 'wip');
if ( ! defined( 'WIP_VERSION' ) ) define('WIP_VERSION', '1.0');
//define( 'GOOGLE_MAP_API_KEY', 'AIzaSyBdlczEuxYRH-xlD_EZH4jv0naeVT1JaA4&#038' );

// Post types constants.
define( 'COURSE_WIP_NAME', 'product' );
define( 'COURSE_WIP_TAXONOMY', 'product_cat' );
define( 'COURSE_WIP_SINGULAR', __( 'Product', 'webideaperu' ) );
define( 'COURSE_WIP_PLURAL', __( 'Products', 'webideaperu' ) );

define( 'VIDEO_WIP_NAME', 'video' );
define( 'VIDEO_WIP_TAXONOMY', 'video_cat' );
define( 'VIDEO_WIP_SINGULAR', __( 'Video', 'webideaperu' ) );
define( 'VIDEO_WIP_PLURAL', __( 'Videos', 'webideaperu' ) );

define( 'SPECIALIST_WIP_NAME', 'specialist' );
define( 'SPECIALIST_WIP_TAXONOMY', 'specialist_cat' );
define( 'SPECIALIST_WIP_SINGULAR', __( 'Especialista', 'webideaperu' ) );
define( 'SPECIALIST_WIP_PLURAL', __( 'Especialistas', 'webideaperu' ) );

define( 'ADVISER_WIP_NAME', 'adviser' );
define( 'ADVISER_WIP_TAXONOMY', 'adviser_cat' );
define( 'ADVISER_WIP_SINGULAR', __( 'Asesor', 'webideaperu' ) );
define( 'ADVISER_WIP_PLURAL', __( 'Asesores', 'webideaperu' ) );

// include post types.
require_once WIP_PLUGIN_PATH . '/entities/class-wip-product.php';

require_once WIP_PLUGIN_PATH . '/entities/class-wip-video.php';

require_once WIP_PLUGIN_PATH . '/entities/class-wip-specialist.php';

require_once WIP_PLUGIN_PATH . '/entities/class-wip-adviser.php';

// Initialize post types.
$wip_product = new Wip_Product();

$wip_video = new Wip_Video();

$wip_specialist = new Wip_Specialist();

$wip_adviser = new Wip_Adviser();

/**
 * Registration hooks.
 */
register_activation_hook( __FILE__, array( $wip_product, 'wip_product_on_activation' ) );


//add_filter( 'woocommerce_register_post_type_product', 'wip_custom_post_type_product' );
function wip_custom_post_type_product( $args ) {
	$labels = wip_get_cpt_labels( COURSE_WIP_SINGULAR, COURSE_WIP_PLURAL );

	$args['labels'] = $labels;
	return $args;
}

function wip_get_cpt_labels( $single, $plural ) {
	$labels = array(
	   'name' => $plural,
	   'singular_name' => $single,
	   'menu_name' => $plural,
	   'add_new' => 'Add '.$single,
	   'add_new_item' => 'Add New '.$single,
	   'edit' => 'Edit',
	   'edit_item' => 'Edit '.$single,
	   'new_item' => 'New '.$single,
	   'view' => 'View '.$plural,
	   'view_item' => 'View '.$single,
	   'search_items' => 'Search '.$plural,
	   'not_found' => 'No '.$plural.' Found',
	   'not_found_in_trash' => 'No '.$plural.' Found in Trash',
	   'parent' => 'Parent '.$single
	);
	return $labels;
}

add_action( 'wp_enqueue_scripts', 'wip_scripts' );
function wip_scripts() {
	wp_enqueue_style( 'wip-custom-css', WIP_PLUGIN_URL . "assets/custom.css", false, WIP_VERSION, 'all' );
	
	global $DC_Woodle;
	//echo '<pre>'; var_dump( get_class_methods( $DC_Woodle->enrollment ) ); echo '</pre>';
	/*
	if( is_front_page() ) {
		wp_enqueue_script( 'modals-js', WIP_PLUGIN_URL . 'js/modals.js', array('jquery'), WIP_VERSION, true);
		wp_enqueue_style( 'modals-css', WIP_PLUGIN_URL . "css/modals.css", false, WIP_VERSION, 'all' );
	}*/
}


//add_action( 'init', 'wip_remove_my_action', 11 );
function  wip_remove_my_action() {
	global $DC_Woodle;
	remove_action( 'woocommerce_after_shop_loop_item_title', array( $DC_Woodle->enrollment, 'add_dates_with_product' ) );
}

add_action( 'astra_woo_shop_after_summary_wrap', 'wip_dates_in_product');
function wip_dates_in_product() {
	global $product;
	$satrtdate = get_post_meta($product->get_id(), '_course_startdate', true);
	$enddate = get_post_meta($product->get_id(), '_course_enddate', true);
	if(woodle_get_settings( 'wc_product_dates_display', 'dc_woodle_general' )) {
		if($satrtdate)
			echo '<div class="astra-shop-bottom-wrap">' . date('d F Y', $satrtdate) . '</div>';
		
	}
}

function convert_spanish_date( $date, $format = '') {
	if( $date <= 0 ) return null;

	if($format == 'tiny') {
		return 	date( 'd\/m\/Y', $date );
	} else {
		$day = date( 'd', $date );
		$m = date( 'n', $date );
		if ($m==1)  { $month = "Enero"; }
		if ($m==2)  { $month = "Febrero"; }
		if ($m==3)  { $month = "Marzo"; }
		if ($m==4)  { $month = "Abril"; }
		if ($m==5)  { $month = "Mayo"; }
		if ($m==6)  { $month = "Junio"; }
		if ($m==7)  { $month = "Julio"; }
		if ($m==8)  { $month = "Agosto"; }
		if ($m==9)  { $month = "Setiembre"; }
		if ($m==10) { $month = "Octubre"; }
		if ($m==11) { $month = "Noviembre"; }
		if ($m==12) { $month = "Diciembre"; };
		$year = date( 'Y', $date );

		return ( $day . ' de ' . $month . ' del ' . $year );	
	}
}

if (!function_exists('get_custom_meta')) {
    function get_custom_meta($wpid, $fieldname, $tablename)
    {
        global $wpdb;
        $result = $wpdb->get_var('select ' . $fieldname . ' from ' . $tablename . ' where ID = ' . $wpid, ARRAY_A);
        return $result;

    }
}

/* Profile customization */
add_action('show_user_profile', 'my_user_profile_edit_action');
add_action('edit_user_profile', 'my_user_profile_edit_action');
add_action('personal_options_update', 'my_user_profile_update_action' );
add_action('edit_user_profile_update', 'my_user_profile_update_action' );

function my_user_profile_edit_action($user) {
	$professional_title = get_user_meta( $user->ID, 'professional_title', true );
	?>
	<h3>Infomación Extra</h3>
	<table class="form-table">
		<tr>
			<th><label for="professional_title">Cargo o Título Profesional</label></th>
			<td>
				<textarea rows="10" cols="450" name="professional_title" id="professional_title"  class="regular-text" /><?php echo esc_textarea( $professional_title ); ?></textarea>
			</td>
		</tr>
	</table>
	<?php 
}

function my_user_profile_update_action($user_id) {	
	if ( ! current_user_can( 'edit_user', $user_id ) ) {
		return false;
	}
	if ( empty( $_POST['professional_title'] ) ) {
		return false;
	}

	update_user_meta($user_id, 'professional_title', sanitize_textarea_field($_POST['professional_title']));
}

/*
add_filter( 'wpcf7_form_tag', array( $this, 'dynamic_field_values' ), 10, 2);
function dynamic_field_values ( $tag, $unused ) {
	if ( $tag['name'] != 'your-name' )
		return $tag;

	global $post;
	
	if( empty( $post->ID ) )
		return $tag;

	$tag['raw_values'][] = $post->post_title;
	$tag['values'][] = $post->post_title;

	/*$args = array (
		'numberposts'   => -1,
		'post_type'     => LP_COURSE_CPT,
		'orderby'       => 'title',
		'order'         => 'ASC',
	);

	$custom_posts = get_posts($args);

	if ( ! $custom_posts )
		return $tag;

	foreach ( $custom_posts as $custom_post ) {

		$tag['raw_values'][] = $custom_post->post_title;
		$tag['values'][] = $custom_post->post_title;
		$tag['labels'][] = $custom_post->post_title;

	}

	return $tag;

} */