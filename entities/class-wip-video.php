<?php
/**
 * Entity File
 *
 * @package entities
 */

/**
 * Wip_Video - "Video entity, post type and stuff
 */
class Wip_Video {

	/**
	 * Table name in 
	 *
	 *
	 * @var string
	 */
	private $wip_video_table;

    private $label_meta_id = VIDEO_WIP_NAME . '_meta_id';

	/**
	 * Label for is 
	 *
	 * @var string
	 */
	private $label_sections = 'sections_data';

	/**
	 * __construct
	 */
	public function __construct() {
		global $wpdb;
		$this->wip_video_table = $wpdb->prefix . 'wip_' . VIDEO_WIP_NAME;

		add_action( 'init', array( $this, 'wip_video_register_pt' ), 0 );
		//add_filter( 'rwmb_meta_boxes', array( $this, 'wip_video_get_meta_box' ) );
		add_action( 'save_post_' . VIDEO_WIP_NAME, array( $this, 'wip_video_save_post') );
	}

	/**
	 * Register table Video Content
	 *
	 * @return void
	 */
	public function wip_video_on_activation() {
		global $wpdb;
		$tbl_idxboost_tools = $this->wip_video_table;

		/*create table*/
		if ( $wpdb->get_var( "SHOW TABLES LIKE '$tbl_idxboost_tools'" ) !== $tbl_idxboost_tools ) {
			$charset_collate = $wpdb->get_charset_collate();

			$sql = "CREATE TABLE $tbl_idxboost_tools (
				`{$this->label_meta_id}` MEDIUMINT(11) unsigned NOT NULL AUTO_INCREMENT, 
				`ID` MEDIUMINT(11) unsigned NOT NULL, 
				`{$this->label_sections}` TEXT, 
				PRIMARY KEY (`{$this->label_meta_id}`)
			) $charset_collate;";
			require_once ABSPATH . 'wp-admin/includes/upgrade.php';
			dbDelta( $sql );
		}
	}

	/**
	 * Register "Video" post type
	 *
	 * @return void
	 */
	public function wip_video_register_pt() {

		$labels = array(
			'name'                  => VIDEO_WIP_PLURAL,
			'singular_name'         => VIDEO_WIP_SINGULAR,
			'menu_name'             => VIDEO_WIP_PLURAL,
			'name_admin_bar'        => VIDEO_WIP_SINGULAR,
			'archives'              => __( 'Item Archives', 'idxboost' ),
			'attributes'            => __( 'Item Attributes', 'idxboost' ),
			'parent_item_colon'     => __( 'Parent Item:', 'idxboost' ),
			'all_items'             => /* translators: %s: plural post type name */
				sprintf( __( 'All %s', 'idxboost' ), VIDEO_WIP_PLURAL ),
			'add_new_item'          => /* translators: %s: singular post type name */
				sprintf( __( 'Add New %s', 'idxboost' ), VIDEO_WIP_SINGULAR ),
			'add_new'               => __( 'Add New', 'idxboost' ),
			'new_item'              => /* translators: %s: singular post type name */
				sprintf( __( 'New %s', 'idxboost' ), VIDEO_WIP_SINGULAR ),
			'edit_item'             => /* translators: %s: singular post type name */
				sprintf( __( 'Edit %s', 'idxboost' ), VIDEO_WIP_SINGULAR ),
			'update_item'           => /* translators: %s: singular post type name */
				sprintf( __( 'Update %s', 'idxboost' ), VIDEO_WIP_SINGULAR ),
			'view_item'             => /* translators: %s: singular post type name */
				sprintf( __( 'View %s', 'idxboost' ), VIDEO_WIP_SINGULAR ),
			'view_items'            => /* translators: %s: plural post type name */
				sprintf( __( 'View %s', 'idxboost' ), VIDEO_WIP_PLURAL ),
			'search_items'          => /* translators: %s: singular post type name */
				sprintf( __( 'Search %s', 'idxboost' ), VIDEO_WIP_SINGULAR ),
			'not_found'             => __( 'Not found', 'idxboost' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'idxboost' ),
			'featured_image'        => __( 'Featured Image', 'idxboost' ),
			'set_featured_image'    => __( 'Set featured image', 'idxboost' ),
			'remove_featured_image' => __( 'Remove featured image', 'idxboost' ),
			'use_featured_image'    => __( 'Use as featured image', 'idxboost' ),
			'insert_into_item'      => /* translators: %s: singular post type name */
				sprintf( __( 'Insert into %s', 'idxboost' ), VIDEO_WIP_SINGULAR ),
			'uploaded_to_this_item' => __( 'Uploaded to this item', 'idxboost' ),
			'items_list'            => /* translators: %s: plural post type name */
				sprintf( __( '%s list', 'idxboost' ), VIDEO_WIP_PLURAL ),
			'items_list_navigation' => /* translators: %s: plural post type name */
				sprintf( __( '%s list navigation', 'idxboost' ), VIDEO_WIP_PLURAL ),
			'filter_items_list'     => /* translators: %s: plural post type name */
				sprintf( __( 'Filter %s list', 'idxboost' ), VIDEO_WIP_PLURAL ),
		);

		$rewrite = array(
			'slug'       => '/acta',
			'with_front' => true,
			'pages'      => true,
			'feeds'      => true,
		);

		$args = array(
			'label'               => VIDEO_WIP_SINGULAR,
			'labels'              => $labels,
			'supports'            => array( 'title', 'editor', 'thumbnail', 'page-attributes' ),
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'menu_position'       => 5,
			'menu_icon'           => 'dashicons-format-video',
			'show_in_admin_bar'   => true,
			'show_in_nav_menus'   => true,
			'can_export'          => true,
			'has_archive'         => true,
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'rewrite'             => $rewrite,
			'capability_type'     => 'page',
		);
		register_post_type( VIDEO_WIP_NAME, $args );
	}

    public function getYoutubeEmbedUrl($url){
        $shortUrlRegex = '/youtu.be\/([a-zA-Z0-9_]+)\??/i';
        $longUrlRegex = '/youtube.com\/((?:embed)|(?:watch))((?:\?v\=)|(?:\/))(\w+)/i';
    
        if (preg_match($longUrlRegex, $url, $matches)) {
            $youtube_id = $matches[count($matches) - 1];
        }
    
        if (preg_match($shortUrlRegex, $url, $matches)) {
            $youtube_id = $matches[count($matches) - 1];
        }
        return 'https://www.youtube.com/embed/' . $youtube_id ;
    }

	/**
	 * Create meta boxes in post type "video"
	 *
	 * @param array $meta_boxes Register all meta boxes and fields.
	 * @return array
	 */
	public function wip_video_get_meta_box( $meta_boxes ) {
		$meta_boxes[] = array(
			'id'           => VIDEO_WIP_NAME . '_info_box',
			'title'        => /* translators: %s: Loft singular name */
				sprintf( __( '%s Information', 'idxboost' ), VIDEO_WIP_SINGULAR ),
			'post_types'   => array( VIDEO_WIP_NAME ),
			'context'      => 'advanced',
			'priority'     => 'default',
			'storage_type' => 'custom_table',    // Important.
			'table'        => $this->wip_video_table, // Your custom table name.
			'fields'       => array(
				array(
					'type'   => 'group',
					'id'     => $this->label_sections,
					'name'   => esc_html__( 'Add sections', 'idxboost' ),
					'class'  => 'large_box_class',
					'clone'  => true,
					'fields' => array(
							array(
								'type'  => 'text',
								'id'    => 'title',
								'name'  => esc_html__( 'Title', 'idxboost' ),
							),
							array(
								'type'    => 'select',
								'id'      => 'type',
								'name'    => esc_html__( 'Type section', 'idxboost' ),
								'options' => array(
									'map'  => 'Map',
									'text' => 'Text',
								),
								// Placeholder text
								'placeholder'     => 'Select a type',
							),
							array(
								'type'    => 'wysiwyg',
								'id'      => 'text',
								'name'    => esc_html__( 'Text', 'idxboost' ),
								'hidden'  => array( 'type', '!=', 'text' ),
							),
					),
				),
			),
		);
		return $meta_boxes;
	}

	/**
	 * Save post Callback
	 *
	 * @param int $post_id Current Post ID.
	 * @return void
	 */
	public function wip_video_save_post( $post_id ) {
		// Return if autosave.
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}

		// Check the user's permissions.
		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return;
		}

		// Delete innecesary post meta.
		delete_post_meta( $post_id, '_edit_last' );
	}
}