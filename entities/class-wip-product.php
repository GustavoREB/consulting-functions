<?php
/**
 * Entity File
 *
 * @package entities
 */

/**
 * Wip_Product - "Product entity, post type and stuff
 */
class Wip_Product {
	private $wip_product_table;

	private $label_meta_id = COURSE_WIP_NAME . '_meta_id';

	private $label_startdate = 'startdate';
	private $label_enddate = 'enddate';
	private $label_duration = 'duration';
	private $label_temary = 'temary';
	private $label_schedules = 'schedules';
	private $label_place = 'place';
	private $label_sessions = 'sessions';
	private $label_addressed_to = 'addressed_to';
	private $label_benefits = 'benefits';
	private $label_topics = 'topics';
	private $label_teachers = 'teachers';
	private $label_certificate = 'certificate';
	private $label_adviser = 'adviser';
    private $label_form = 'form';

	/**
	 * __construct
	 */
	public function __construct() {
		global $wpdb;
		$this->wip_product_table = $wpdb->prefix . 'wip_' . COURSE_WIP_NAME;

		//add_filter( 'rwmb_meta_boxes', array( $this, 'wip_product_get_meta_box' ) );
		//add_action( 'init', array( $this, 'tg_product_taxonomy' ), 0 );
		add_action( "added_post_meta", array( $this, 'wip_product_update_date' ), 10, 4 );
		add_action( "updated_post_meta", array( $this, 'wip_product_update_date' ), 10, 4 );
		//add_action( 'save_post_' . COURSE_WIP_NAME, array( $this, 'wip_product_save_post') );

		/* customs columns listing post type */
		//add_filter(sprintf('manage_%s_posts_columns', COURSE_WIP_NAME), array($this, 'wip_product_manage_columns'));
		//add_filter(sprintf('manage_%s_posts_custom_column', COURSE_WIP_NAME), array($this, 'wip_product_manage_post_columns'), 10, 2);
		//add_filter(sprintf('manage_edit-%s_sortable_columns', COURSE_WIP_NAME), array($this,'wip_product_sortable_columns'));	
		//add_action( 'pre_get_posts', array($this, 'wip_product_custom_orderby'));

	}

	/**
	 * Register taxonomy "category_product"
	 *
	 * @return void
	 */
	public function tg_product_taxonomy() {
		$labels = array(
			'name'                       => _x( 'Product types', 'Taxonomy General Name', 'idxboost' ),
			'singular_name'              => _x( 'Product type', 'Taxonomy Singular Name', 'idxboost' ),
			'menu_name'                  => __( 'Product type', 'idxboost' ),
			'all_items'                  => __( 'All Items', 'idxboost' ),
			'parent_item'                => __( 'Parent Item', 'idxboost' ),
			'parent_item_colon'          => __( 'Parent Item:', 'idxboost' ),
			'new_item_name'              => __( 'New Item Name', 'idxboost' ),
			'add_new_item'               => __( 'Add New Item', 'idxboost' ),
			'edit_item'                  => __( 'Edit Item', 'idxboost' ),
			'update_item'                => __( 'Update Item', 'idxboost' ),
			'view_item'                  => __( 'View Item', 'idxboost' ),
			'separate_items_with_commas' => __( 'Separate items with commas', 'idxboost' ),
			'add_or_remove_items'        => __( 'Add or remove items', 'idxboost' ),
			'choose_from_most_used'      => __( 'Choose from the most used', 'idxboost' ),
			'popular_items'              => __( 'Popular Items', 'idxboost' ),
			'search_items'               => __( 'Search Items', 'idxboost' ),
			'not_found'                  => __( 'Not Found', 'idxboost' ),
			'no_terms'                   => __( 'No items', 'idxboost' ),
			'items_list'                 => __( 'Items list', 'idxboost' ),
			'items_list_navigation'      => __( 'Items list navigation', 'idxboost' ),
		);

		$rewrite = array(
			'slug'       => COURSE_WIP_TAXONOMY,
			'with_front' => true,
		);

		$args = array(
			'labels'             => $labels,
			'hierarchical'       => true,
			'public'             => true,
			'show_ui'            => true,
			'show_in_quick_edit' => false,
			'meta_box_cb'        => false,
			'show_admin_column'  => true,
			'show_in_nav_menus'  => true,
			'show_tagcloud'      => true,
			'rewrite'            => $rewrite,
		);
		register_taxonomy( COURSE_WIP_TAXONOMY, array( COURSE_WIP_NAME ), $args );

	}

	/**
	 * Register table product
	 *
	 * @return void
	 */
	public function wip_product_on_activation() {
		global $wpdb;
		$tbl_idxboost_tools = $this->wip_product_table;
		
		/*create table*/
		if ( $wpdb->get_var( "SHOW TABLES LIKE '$tbl_idxboost_tools'" ) !== $tbl_idxboost_tools ) {
			$charset_collate = $wpdb->get_charset_collate();

			$sql = "CREATE TABLE $tbl_idxboost_tools (
				`{$this->label_meta_id}` MEDIUMINT(11) unsigned NOT NULL AUTO_INCREMENT, 
				`ID` MEDIUMINT(11) unsigned NOT NULL, 
				`{$this->label_startdate}` TEXT, 
				`{$this->label_enddate}` TEXT, 
                `{$this->label_duration}` MEDIUMINT(11), 
                `{$this->label_temary}` TEXT, 
                `{$this->label_schedules}` TEXT, 
                `{$this->label_place}` TEXT,
				`{$this->label_sessions}` MEDIUMINT(11),
                `{$this->label_addressed_to}` TEXT, 
                `{$this->label_benefits}` TEXT, 
                `{$this->label_topics}` TEXT, 
                `{$this->label_teachers}` TEXT,
                `{$this->label_certificate}` TEXT,
                `{$this->label_adviser}` TEXT,
                `{$this->label_form}` TEXT,
				PRIMARY KEY (`{$this->label_meta_id}`)
			) $charset_collate;";
			require_once ABSPATH . 'wp-admin/includes/upgrade.php';
			dbDelta( $sql );
		}
	}

	public function wip_product_navigation_tabs() {
		$tabs = array(
			'description'   => array(
				'label' => 'Descripción',
				'icon'  => 'dashicons-welcome-write-blog', // Dashicon
				),
			'addressed' => array(
				'label' => 'Dirigido a',
				   'icon'  => 'dashicons-email', // Dashicon
				),
			'benefits'   => array(
				'label' => 'Beneficios',
				'icon'  => 'dashicons-admin-generic', // Dashicon
				),
			'general'   => array(
				'label' => 'Contenido',
				'icon'  => 'dashicons-admin-generic', // Dashicon
				),	
			'instructor'    => array(
				'label' => 'Información extra',
				'icon'  => 'dashicons-businessman', // Dashicon
				),
			);
		return $tabs;
	}

	/**
	 * Create meta boxes in post type "product"
	 *
	 * @param array $meta_boxes Register all meta boxes and fields.
	 * @return array
	 */
	public function wip_product_get_meta_box( $meta_boxes ) {
		$tabs = $this->wip_product_navigation_tabs();

		$meta_boxes[] = array(
			'id'           => COURSE_WIP_NAME . '_info_box',
			'title' => __( 'Información adicional del curso', 'webideaperu' ),
			'post_types'   => array( COURSE_WIP_NAME ),
			'context'      => 'advanced',
			'priority'     => 'default',
			'tabs'         => $tabs,
	        'tab_style'    => 'left',
			'storage_type' => 'custom_table',    // Important.
			'table'        => $this->wip_product_table, // Your custom table name.
			'fields'       => array(
				array(
					'name'     => 'Inicio',
					'id'       => $this->label_startdate,
					'type'     => 'text',
					'tab'      => 'description',
					'desc'     => __( 'Fecha de inicio.', 'webideaperu' ),
					'readonly' => true, 
				),
				array(
					'name'     => 'Fin',
					'id'       => $this->label_enddate,
					'type'     => 'text',
					'tab'      => 'description',
					'desc'     => __( 'Fecha de fin.', 'webideaperu' ),
					'readonly' => true, 
				),
				array(
					'name'  => 'Horas lectivas',
					'id'    => $this->label_duration,
					'type'  => 'number',
					'tab'   => 'description',
					'desc' => __( 'Número de horas que impartirá el curso.', 'webideaperu' ),
				),
				array(
					'name'	=> 'Horarios',
					'id'    => $this->label_schedules,
					'type'	=> 'textarea',
					'cols'	=> 5,
					'tab'	=> 'description',
					'desc' => __( 'Horarios de los cursos.', 'webideaperu' ),
				),
				array(
					'name'	=> 'Lugar',
					'id'	=> $this->label_place,
					'type'	=> 'textarea',
					'cols'	=> 5,
					'tab'	=> 'description',
					'desc' => __( 'Lugar en que se impartira el curso.', 'webideaperu' ),
				),
				array(
					'name'	=> 'Temario',
					'id'    => $this->label_temary,
					'type'  => 'file_advanced',
					'max_file_uploads' => 1,
					'tab'	=> 'description',
					'desc' => __( 'Temario del curso en PDF.', 'webideaperu' ),
				),
				array(
					'name'  => 'Sesiones',
					'id'    => $this->label_sessions,
					'type'  => 'number',
					'tab'   => 'description',
					'desc' => __( 'Número de horas o sesiones que impartirá el curso.', 'webideaperu' ),
				),
				array(
					'name'  => 'Video',
					'id'    => $this->label_video,
					'type'  => 'text',
					'tab'   => 'description',
					'desc' => __( 'Número de horas o sesiones que impartirá el curso.', 'webideaperu' ),
				),
				array(
					'name' => 'Dirigido a',
					'id'   => $this->label_addressed_to,
					'type' => 'textarea',
					'cols'  => 5,
					'clone'=> true,
					'tab'  => 'addressed',
					'desc' => __( 'Para quiénes va dirigido el curso. <html>', 'webideaperu' ),
				),
				array(
					'name' => 'Beneficios',
					'id'   => $this->label_benefits,
					'type' => 'textarea',
					'cols'  => 5,
					'clone'=> true,
					'tab'  => 'benefits',
					'desc' => __( 'Beneficios que brinda el curso. <html>', 'webideaperu' ),
				),
				array(
					'name' => 'Contenido',
					'id'   => $this->label_topics,
					'type' => 'group',
					'fields' => array(
						array(
							'name'     => 'Titulo',
							'id'       => 'titulo',
							'type'     => 'text',
							'desc'     => __( 'Titulo', 'webideaperu' ),
						),
						array(
							'name'     => 'Descripcion',
							'id'       => 'descripction',
							'type' => 'wysiwyg',
							'options' => array(
								'textarea_rows' => 4,
								'media_buttons' => false,
								'tinymce'		=> true		
							),
							'desc'     => __( 'Descripción.', 'webideaperu' ),
						),
					),
					'tab'  => 'general',
					'clone' => true,
					'sort_clone' => true,
					'desc' => __( 'Contenido del curso. <html>', 'webideaperu' ),
				),
				array(
					'name' => 'Asesor',
					'id'   => $this->label_adviser,
					'type'        => 'user',
					'field_type'  => 'select_advanced',
					'placeholder' => __( 'Selecciona un asesor', 'WIP_LANG' ),
					'query_args'  => array(),
					'tab'	=> 'instructor',
					'desc' => __( 'Asesor que imparte el curso', 'WIP_LANG' ),
				),
				array(
					'name' => 'Especialistas',
					'id'   => $this->label_teachers,
					'type'        => 'user',
					'field_type'  => 'select_advanced',
					'placeholder' => __( 'Selecciona un instructor', 'WIP_LANG' ),
					'query_args'  => array(),
					'tab'	=> 'instructor',
					'desc' => __( 'Instructor que imparte el curso', 'WIP_LANG' ),
				),
				array(
					'name' => 'Certificado',
					'id'   => $this->label_certificate,
					'type' => 'wysiwyg',
					'tab'	=> 'instructor',
					'desc' => __( 'Beneficios que brinda el curso. <html>', 'webideaperu' ),
				),
				array(
					'name'  => 'Formulario',
					'id'    => $this->label_form,
					'type'  => 'post',
					'post_type' => 'wpcf7_contact_form',
					'field_type'  => 'select_advanced',
					'tab'   => 'instructor',
					'desc'  => __( 'Selecciona el formulario.', 'webideaperu' ),
				),
			),
		);
		
		return $meta_boxes;
	}

	/**
	 * Save post Callback
	 *
	 * @param int $post_id Current Post ID.
	 * @return void
	 */
	public function wip_product_save_post( $post_id ) {
		// Return if autosave.
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}

		// Check the user's permissions.
		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return;
		}

		// Delete innecesary post meta.
		delete_post_meta( $post_id, '_edit_last' );
	}

	public function wip_product_manage_columns( $posts_columns = array() ) {
		$posts_columns['is_polygon'] = 'Is Polygon?' ;
		return $posts_columns;
	}
	
	public function wip_product_manage_post_columns( $column_name, $ID ) {
		switch ($column_name) {
			case 'is_polygon':
				$is_polygon = get_kafka_meta($ID, 'is_polygon', $this->wip_product_table);
				if( (int) $is_polygon == 1 ):
					echo 'Yes';
				else: 
					echo 'No';
				endif;
			break;
		}
	}

	/*
	public function wip_product_sortable_columns( $columns ) {
		$columns['is_polygon'] = 'is_polygon';

		return $columns;
	}

	public function wip_product_custom_orderby( $query ) {
		if ( ! is_admin() )
			return;

		$orderby = $query->get( 'orderby');

		if ( 'is_polygon' == $orderby ) {
			$query->set( 'meta_key', 'is_polygon' );
			$query->set( 'orderby', 'meta_value_num' );
		}
	} */

	public function wip_product_update_date( $meta_id, $post_id, $meta_key, $meta_value ) {		
		if ( ! in_array( $meta_key, array( '_course_startdate', '_course_enddate' ) ) ) {
			return;
		}

		global $wpdb;
		$convert_date = convert_spanish_date( $meta_value );
		$convert_key = str_replace( '_course_', '', $meta_key );

		//$current_meta_id = $wpdb->get_var( 'SELECT ' . $this->label_meta_id . ' FROM ' . $this->wip_product_table . ' WHERE ID = ' . $post_id );
		
		$current_meta_id = update_post_meta( $post_id, $convert_key, $convert_date );		

		if( $current_meta_id === FALSE ) var_dump( 'ERROR UPDATE' );

		/*
		if( ! isset( $current_meta_id ) ) {
			$wpdb->insert(
				$this->wip_product_table,
				array(
					'ID' 	     => $post_id,
					$convert_key => $convert_date,   // string
				),
				array(
					'%d', '%s'
				)
			);
			$current_meta_id = $wpdb->insert_id;
			if( empty( $current_meta_id ) ) { 
				var_dump( 'Error update' );
				die;
			}
			if( $wpdb->show_errors() != 1 ) var_dump( $wpdb->show_errors() );
		} else {
			$wpdb->update(
				$this->wip_product_table,
				array(
					$convert_key => $convert_date,   // string
				),
				array( $this->label_meta_id => $current_meta_id ), 
			);
			if( $wpdb->show_errors() != 1 ) var_dump( $wpdb->show_errors() );
		} */
	}
}

